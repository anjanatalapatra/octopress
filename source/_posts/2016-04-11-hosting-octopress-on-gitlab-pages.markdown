---
layout: post
title: "Hosting Octopress on GitLab Pages"
date: 2016-04-11 08:22:48 +0300
comments: true
categories: [GitLab, Pages]
---

Welcome to My Research Page!

I am a researcher at Texas A&M University
#[GitLab.com][] and deployed with [GitLab Pages][pages].

Read more at <https://gitlab.com/pages/octopress>.


[GitLab.com]: https://about.gitlab.com/gitlab-com
[pages]: https://pages.gitlab.io
